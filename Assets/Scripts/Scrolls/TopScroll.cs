﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TopScroll : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	bool scrolling = false;
	public GameObject mainCamera;

	public void OnPointerEnter(PointerEventData eventData)
	{
		scrolling = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		scrolling = false;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (scrolling)
		{
			mainCamera.GetComponent<CameraMotion>().ScrollUp();
		}	
	}
}
