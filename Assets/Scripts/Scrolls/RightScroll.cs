﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RightScroll : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

	bool scrolling = false;
	public GameObject mainCamera;
	float enterTime = 0.0f;

	public void OnPointerEnter(PointerEventData eventData)
	{
		enterTime = Time.time;
		scrolling = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		scrolling = false;
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (scrolling && (Time.time - enterTime > 0.1f))
		{
			mainCamera.GetComponent<CameraMotion>().ScrollRight();
		}
	}
}
