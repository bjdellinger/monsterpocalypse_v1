﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BottomScroll : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

	bool scrolling = false;
	public GameObject mainCamera;

	public void OnPointerEnter(PointerEventData eventData)
	{
		scrolling = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		scrolling = false;
	}
	

	// Update is called once per frame
	void Update()
	{
		if (scrolling)
		{
			mainCamera.GetComponent<CameraMotion>().ScrollDown();
		}
	}
}
