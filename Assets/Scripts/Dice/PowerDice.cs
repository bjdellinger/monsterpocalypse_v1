using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDice : Dice {
	
	void Start() {
		base.DoInitialization ();
		faces = new int[6];
		faces [0] = BLANK;
		faces [1] = STRIKE;
		faces [2] = STRIKE;
		faces [3] = STRIKE;
		faces [4] = STRIKE;
		faces [5] = SUPER_STRIKE;
	}

}
