using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour {
	protected const int BLANK = 0;
	protected const int STRIKE = 1;
	protected const int SUPER_STRIKE = 2;

	protected int[] faces = {BLANK, STRIKE, SUPER_STRIKE};
	protected int face = 1;
	protected SpriteRenderer rend;
	public Sprite[] spriteFaces = new Sprite[3];


	void Start() {
		DoInitialization ();
	}

	protected void DoInitialization() {
		rend = GetComponent<SpriteRenderer> ();
	}

	public void Roll(System.Random rand) {
		face = rand.Next(faces.Length);
		rend.sprite = spriteFaces [faces [face]];
	}

	public int GetValue() {
		return faces [face];
	}

	public void GrayOut() {
		rend.color = new Color (1f, 1f, 1f, 0.4f);
	}

	public void UnGray() {
		rend.color = new Color (1f, 1f, 1f, 1f);
	}

}
