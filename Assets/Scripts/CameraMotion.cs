﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotion : MonoBehaviour {

	public float zoomSpeed;
	public float targetOrtho;
	public float smoothSpeed;
	public float minOrtho;
	public float maxOrtho;
	public float panSpeed;
	
	void Start() {
		Camera.main.orthographicSize = 9;
		targetOrtho = Camera.main.orthographicSize;
	}

	public void ScrollUp()
	{
		float deltaTime = Time.deltaTime;
		transform.Translate(new Vector3(0, panSpeed * deltaTime, 0));
	}

	public void ScrollDown()
	{
		float deltaTime = Time.deltaTime;
		transform.Translate(new Vector3(0, -panSpeed * deltaTime, 0));
	}

	public void ScrollLeft()
	{
		float deltaTime = Time.deltaTime;
		transform.Translate(new Vector3(-panSpeed * deltaTime, 0, 0));
	}
	
	public void ScrollRight()
	{
		float deltaTime = Time.deltaTime;
		transform.Translate(new Vector3(panSpeed * deltaTime, 0, 0));

	}
	
	void Update () {

		float deltaTime = Time.deltaTime;
		if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < 18)
		{
			ScrollRight();
		}
		if(Input.GetKey(KeyCode.LeftArrow) && transform.position.x > -7)
		{
			ScrollLeft();
		}
		if(Input.GetKey(KeyCode.DownArrow) && transform.position.y > -10)
		{
			ScrollDown();
		}
		if(Input.GetKey(KeyCode.UpArrow) && transform.position.y < 8)
		{
			ScrollUp();
		}
		
		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		if (scroll != 0.0f) {
			targetOrtho -= scroll * zoomSpeed;
			targetOrtho = Mathf.Clamp (targetOrtho, minOrtho, maxOrtho);
		}
		
		Camera.main.orthographicSize = Mathf.MoveTowards (Camera.main.orthographicSize, targetOrtho, smoothSpeed * deltaTime);
	}
}
