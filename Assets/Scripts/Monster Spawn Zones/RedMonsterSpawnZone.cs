﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RedMonsterSpawnZone : MonsterSpawnZone {

	// Use this for initialization
	void Start () {
		base.DoInitialization ();
		team = Toolbox.RED_PLAYER;
	}
}
