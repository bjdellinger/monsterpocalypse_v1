﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BlueMonsterSpawnZone : MonsterSpawnZone{

	// Use this for initialization
	void Start () {
		base.DoInitialization ();
		team = Toolbox.BLUE_PLAYER;
	}

}
