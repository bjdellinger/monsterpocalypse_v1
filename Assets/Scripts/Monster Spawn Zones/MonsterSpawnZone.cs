﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MonsterSpawnZone : SpawnZone, IPointerClickHandler, IPointerEnterHandler {
	protected int team = 0;
	// Use this for initialization
	void Start () {
		base.DoInitialization ();
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		if (tbox.currentPhase != Toolbox.PHASE_CHOOSE_MONSTER_SPAWN) {
			return;
		} else if (team != tbox.currentPlayer) {
			return;
		}
		rend.enabled = false;
		tbox.Spawn (Toolbox.TERRA_KHAN_ALPHA, transform.position);
		tbox.UpdatePhase (true);
	}

	#endregion

	new public void OnPointerEnter (PointerEventData eventData) {
		// If it's the wrong phase, or the wrong team, quit
		if (tbox.currentPhase != Toolbox.PHASE_CHOOSE_MONSTER_SPAWN) {
			return;
		} else if (team != tbox.currentPlayer) {
			return;
		}
		base.OnPointerEnter (eventData);
	}
}
