﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Figure {
	
	public int monsterName;
	public int cost;
	public int speed;
	public int maxBlast;
	public int blastBoost;
	public int maxBrawl;
	public int brawlBoost;
	public int maxPower;
	public int powerBoost;
	public bool hasMoved = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	public bool CanBlast()
	{
		return maxBlast > 0;
	}

	public bool CanBrawl()
	{
		return maxBrawl > 0;
	}
}
