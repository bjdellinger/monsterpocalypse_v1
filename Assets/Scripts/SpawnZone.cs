﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpawnZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	protected SpriteRenderer rend;
	protected Toolbox tbox;
	bool fadingIn = false;
	bool mouseInside = false;
	float alpha = 1f;
	float step = 0.05f;


	#region IPointerEnterHandler implementation

	public void OnPointerEnter(PointerEventData eventData)
	{
		mouseInside = true;
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit(PointerEventData eventData)
	{
		DeactivateAnimation();
	}

	#endregion

	public void DeactivateAnimation()
	{
		alpha = 1;
		GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
		fadingIn = true;
		mouseInside = false;

	}

	void Awake()
	{
		rend = GetComponent<SpriteRenderer>();
	}

	// Use this for initialization
	void Start()
	{
		DoInitialization();
	}

	protected void DoInitialization()
	{
		rend.color = new Color(1, 1, 1, 0);
		tbox = GameObject.Find("ToolboxObj").GetComponent<Toolbox>();
	}

	// Update is called once per frame
	void Update()
	{
		UpdateFading();
	}

	void UpdateFading()
	{
		if (!mouseInside)
		{
			return;
		}
		if (fadingIn)
		{
			alpha += step;
			if (alpha >= 1)
			{
				fadingIn = false;
			}
		}
		else
		{
			alpha -= step;
			if (alpha <= 0)
			{
				fadingIn = true;
			}
		}
		rend.color = new Color(1, 1, 1, alpha);
	}

	public void SetActiveZone(bool active)
	{
		// If turning off, make sure you end the animations.
		if (!active)
		{
			DeactivateAnimation();

		}
		gameObject.SetActive(active);
	}

}
