using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class YellowBuildingSpawnZone : BuildingSpawnZone, IPointerClickHandler, IPointerEnterHandler
{
	void Start() {
		buildingColor = Toolbox.YELLOW_BUILDING;
		base.DoInitialization ();
	}


	new public void OnPointerEnter (PointerEventData eventData) {
		if (tbox.remainingGreenZones > 0) {
			return;
		}
		base.OnPointerEnter (eventData);
	}

	new public void OnPointerClick (PointerEventData eventData)
	{
		if ((tbox.currentPhase == Toolbox.PHASE_CHOOSE_BUILDING) 
		    && (tbox.remainingGreenZones == 0)) {
			tbox.remainingYellowZones--;
			base.OnPointerClick(eventData);
		}
	}
}
