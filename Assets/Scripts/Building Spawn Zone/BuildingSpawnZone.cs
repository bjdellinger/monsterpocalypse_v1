﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(SpriteRenderer))]
public class BuildingSpawnZone : SpawnZone, IPointerClickHandler, IPointerEnterHandler {

	protected int buildingColor = 0;


	// Use this for initialization
	void Start () {
		base.DoInitialization ();
	}


	
	#region IPointerClickHandler implementation
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (tbox.currentPhase != Toolbox.PHASE_CHOOSE_BUILDING) {
			return;
		}
		this.SetActiveZone(false);
		tbox.Spawn (Toolbox.APARTMENT_BUILDING, transform.position);
		tbox.UpdatePhase (true);
	}
	
	#endregion

	new public void OnPointerEnter (PointerEventData eventData) {
		// If it's the wrong phase, or the wrong team, quit
		if (tbox.currentPhase != Toolbox.PHASE_CHOOSE_BUILDING) {
			return;
		}
		base.OnPointerEnter (eventData);
	}
}
