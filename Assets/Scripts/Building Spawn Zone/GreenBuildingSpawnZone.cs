using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class GreenBuildingSpawnZone : BuildingSpawnZone, IPointerClickHandler
{
	void Start() {
		buildingColor = Toolbox.GREEN_BUILDING;
		base.DoInitialization ();
	}

	new public void OnPointerClick (PointerEventData eventData)
	{
		if (tbox.currentPhase == Toolbox.PHASE_CHOOSE_BUILDING) {
			tbox.remainingGreenZones--;
		}
		base.OnPointerClick(eventData);
	}
}

