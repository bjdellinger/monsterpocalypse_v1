﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HazyPanel : MonoBehaviour, IPointerClickHandler {

	public Canvas cvas;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			cvas.gameObject.SetActive (false);
		}
		
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		cvas.gameObject.SetActive (false);
	}

	#endregion
}
