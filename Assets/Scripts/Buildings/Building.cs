﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Building: Figure, IPointerClickHandler
{
	public int power;
	public int rubbleType;

	BuildingHandler bHandler;

	public void Start()
	{
		DoInitialization();
	}

	public void DoInitialization()
	{
		bHandler = GameObject.Find("ToolboxObj").GetComponent<BuildingHandler>();
		health = 1;
	}
	
	public void OnPointerClick(PointerEventData eventData)
	{
		bHandler.SelectBuilding(this);
	}
}
