﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildingHandler : MonoBehaviour {

	Toolbox tbox;
	UnitHandler uHandler;
	public List<GameObject> buildings = new List<GameObject>();
	public Sprite[] rubbleTypes = new Sprite[5];
	public GameObject prefabRubble;
	public List<GameObject> rubble = new List<GameObject>();



	public const int RUBBLE_PLAIN = 0;
	public const int RUBBLE_FIRE = 1;
	public const int RUBBLE_RADIATION = 2;
	public const int RUBBLE_ACID = 3;
	public const int RUBBLE_HELL = 4;

	// Use this for initialization
	void Start ()
	{
		tbox = GameObject.Find("ToolboxObj").GetComponent<Toolbox>();
		uHandler = GameObject.Find("ToolboxObj").GetComponent<UnitHandler>();

	}
	// Select target unit, or deselect all units with an argument of null.
	public void SelectBuilding(Building target)
	{
		// If we're choosing the target for a unit attack...
		if (tbox.currentPhase == Toolbox.PHASE_UNIT_ATTACK
			&& tbox.currentAttack == Toolbox.AttackStatus.ChooseTarget)
		{
			//Verify it's a legit target.
			// Is it out of range?
			if (tbox.GetRange(target.gameObject, uHandler.attackLeader.gameObject)
				> uHandler.attackLeader.range)
			{
				return;
			}
			// No to the above? Then it's a legit target.
			uHandler.HandleChooseAttackTarget(target.gameObject);
		}
	}

	public void PlaceRubbleOnMap(Vector2Int nwCorner, int rubbleType)
	{
		if (rubbleType == RUBBLE_PLAIN)
		{
			tbox.buildingMap[nwCorner.x, nwCorner.y] = Toolbox.TERRAIN_RUBBLE_NW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y] = Toolbox.TERRAIN_RUBBLE_NE;
			tbox.buildingMap[nwCorner.x, nwCorner.y - 1] = Toolbox.TERRAIN_RUBBLE_SW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y - 1] = Toolbox.TERRAIN_RUBBLE_SE;
		}
		else if (rubbleType == RUBBLE_ACID)
		{
			tbox.buildingMap[nwCorner.x, nwCorner.y] = Toolbox.TERRAIN_ACID_NW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y] = Toolbox.TERRAIN_ACID_NE;
			tbox.buildingMap[nwCorner.x, nwCorner.y - 1] = Toolbox.TERRAIN_ACID_SW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y - 1] = Toolbox.TERRAIN_ACID_SE;
		}
		else if (rubbleType == RUBBLE_FIRE)
		{
			tbox.buildingMap[nwCorner.x, nwCorner.y] = Toolbox.TERRAIN_FIRE_NW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y] = Toolbox.TERRAIN_FIRE_NE;
			tbox.buildingMap[nwCorner.x, nwCorner.y - 1] = Toolbox.TERRAIN_FIRE_SW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y - 1] = Toolbox.TERRAIN_FIRE_SE;
		}
		else if (rubbleType == RUBBLE_HELL)
		{
			tbox.buildingMap[nwCorner.x, nwCorner.y] = Toolbox.TERRAIN_HELLMOUTH_NW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y] = Toolbox.TERRAIN_HELLMOUTH_NE;
			tbox.buildingMap[nwCorner.x, nwCorner.y - 1] = Toolbox.TERRAIN_HELLMOUTH_SW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y - 1] = Toolbox.TERRAIN_HELLMOUTH_SE;
		}
		else if (rubbleType == RUBBLE_RADIATION)
		{
			tbox.buildingMap[nwCorner.x, nwCorner.y] = Toolbox.TERRAIN_RADIOACTIVE_NW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y] = Toolbox.TERRAIN_RADIOACTIVE_NE;
			tbox.buildingMap[nwCorner.x, nwCorner.y - 1] = Toolbox.TERRAIN_RADIOACTIVE_SW;
			tbox.buildingMap[nwCorner.x + 1, nwCorner.y - 1] = Toolbox.TERRAIN_RADIOACTIVE_SE;
		}
		else
		{
			print("Invalid error type - BuildingHandler/PlaceRubble");
		}
	}

	public void DestroyBuilding(GameObject bldgObj)
	{
		Building bldg = bldgObj.GetComponent<Building>();
		int rubbleType = bldg.rubbleType;
		Vector2Int nwCorner = tbox.GetNWCorner(bldgObj);
		buildings.Remove(bldgObj);
		Destroy(bldgObj);
		PlaceRubbleOnMap(nwCorner, rubbleType);

		GameObject rub = Instantiate(prefabRubble, tbox.TranslateNWCornerToMap(nwCorner), 
			Quaternion.identity);
		rub.GetComponent<SpriteRenderer>().sprite = rubbleTypes[rubbleType];
		rubble.Add(rub);
	}

}
