﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ApartmentBuilding : Building
{

	public void Awake()
	{
		// Placeholders.
		power = 2;
		defense = 4;
		rubbleType = BuildingHandler.RUBBLE_FIRE;
	}
}

