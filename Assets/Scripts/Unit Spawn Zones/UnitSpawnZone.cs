﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitSpawnZone : SpawnZone, IPointerEnterHandler, IPointerClickHandler {
	protected int team = 0;
	protected UnitHandler uHandler;

	// Use this for initialization
	void Start () {
		DoInitialization();
	}

	new protected void DoInitialization()
	{
		base.DoInitialization();
		uHandler = GameObject.Find("ToolboxObj").GetComponent<UnitHandler>();

	}

	new public void OnPointerEnter (PointerEventData eventData) {
		// If it's the wrong phase, or the wrong team, quit
		if (tbox.currentPhase != Toolbox.PHASE_UNIT_SPAWN) {
			return;
		} else if (team != tbox.currentPlayer) {
			return;
		}
		GameObject unit = uHandler.GetSelectedUnit();
		// Disregard if there is no unit selected.
		if (!unit)
		{
			return;
		}
		// If the selected unit has the wrong color, disregard.
		if (team != unit.GetComponent<Unit>().color)
		{
			return;
		}
		// Now highlight.
		base.OnPointerEnter (eventData);

	}

	public void OnPointerClick(PointerEventData eventData)
	{
		// If it's not a unit spawn step, do nothing.
		if (tbox.currentPhase != Toolbox.PHASE_UNIT_SPAWN)
		{
			return;
		}
		// If there's no unit selected, do nothing.
		GameObject unitObj = uHandler.GetSelectedUnit();
		if (!unitObj)
		{
			return;
		}
		// If the thing selected isn't of the right color, do nothing.
		Unit unit = unitObj.GetComponent<Unit>();
		if (unit.color != tbox.currentPlayer)
		{
			return;
		}
		// If the selected unit isn't in the unit bank, do nothing.
		if (!uHandler.UnitInBank(unit.color, unitObj))
		{
			return;
		}
		// If the unit is grayed out, do nothing.
		if (unit.gray)
		{
			return;
		}
		// So we're in the correct phase, and there was a unit chosen,
		// and it was a unit in our color, and it was in the unit bank,
		// and it wasn't gray.
		// Now spawn that unit at that location.
		uHandler.SpawnUnit(unitObj, this.gameObject);
		tbox.instructionText.text = tbox.playerNames[tbox.currentPlayer]
			+ ", choose a unit from your bank.";
	}
}
