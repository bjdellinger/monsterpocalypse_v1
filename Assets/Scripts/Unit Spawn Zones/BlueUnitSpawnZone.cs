﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueUnitSpawnZone : UnitSpawnZone {

	// Use this for initialization
	void Start () {
		base.DoInitialization ();
		team = Toolbox.BLUE_PLAYER;
		
	}
}
