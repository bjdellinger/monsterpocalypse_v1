﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedUnitSpawnZone : UnitSpawnZone {

	// Use this for initialization
	void Start () {
		base.DoInitialization ();
		team = Toolbox.RED_PLAYER;
		
	}
}
