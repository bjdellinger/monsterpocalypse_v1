﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toolbox : MonoBehaviour
{

	public const int PHASE_CHOOSE_BUILDING = 10;
	public const int PHASE_CHOOSE_MONSTER_SPAWN = 30;
	public const int PHASE_UNIT_SPAWN = 50;
	public const int PHASE_UNIT_MOVE = 70;
	public const int PHASE_UNIT_ATTACK = 90;
	public const int PHASE_POWER_UP = 110;
	public const int PHASE_HYPER_UP = 130;
	public const int PHASE_MONSTER_MOVE = 150;
	public const int PHASE_MONSTER_ATTACK = 170;
	public int currentPhase;

	public const int BLUE_PLAYER = 0;
	public const int RED_PLAYER = 1;
	public string[] playerNames = { "Blue Player", "Red Player" };
	public int currentPlayer;
	public int firstPlayer;

	public const int MAP_CELLS = 17;

	public const int UNIT = 0;
	public const int MONSTER = 1;
	public const int BUILDING = 2;

	public const int GREEN_BUILDING = 3;
	public const int YELLOW_BUILDING = 4;

	public const int TOTAL_GREEN_ZONES = 6;
	public const int TOTAL_YELLOW_ZONES = 10;
	public int remainingGreenZones;
	public int remainingYellowZones;

	public const int INDEX_BUILDING_MIN = 0;
	public const int APARTMENT_BUILDING = 0;
	public const int INDEX_BUILDING_MAX = 0;
	public GameObject apartment_building;

	public const int INDEX_MONSTER_MIN = 50;
	public const int TERRA_KHAN_ALPHA = 50;
	public const int INDEX_MONSTER_MAX = 50;
	public GameObject terra_khan_alpha;

	public GameObject[] prototypes = new GameObject[51];
	
	public GameObject[] largeBorders = new GameObject[2];
	public GameObject[] smallBorders = new GameObject[2];

	public const int TERRAIN_EMPTY = 0;
	public const int TERRAIN_ROUGH = 1;
	public const int TERRAIN_WATER = 2;
	public const int TERRAIN_COVER = 4;
	public const int TERRAIN_ROUGHCOVER = 5;
	public const int TERRAIN_BLUE_UNIT = 10;
	public const int TERRAIN_RED_UNIT = 11;
	public const int TERRAIN_BUILDING_NE = 14;
	public const int TERRAIN_BUILDING_NW = 15;
	public const int TERRAIN_BUILDING_SE = 16;
	public const int TERRAIN_BUILDING_SW = 17;
	public const int TERRAIN_BLUE_MONSTER_NE = 18;
	public const int TERRAIN_BLUE_MONSTER_NW = 19;
	public const int TERRAIN_BLUE_MONSTER_SE = 20;
	public const int TERRAIN_BLUE_MONSTER_SW = 21;
	public const int TERRAIN_RED_MONSTER_NE = 22;
	public const int TERRAIN_RED_MONSTER_NW = 23;
	public const int TERRAIN_RED_MONSTER_SE = 24;
	public const int TERRAIN_RED_MONSTER_SW = 25;
	public const int TERRAIN_RUBBLE_NW = 26;
	public const int TERRAIN_RUBBLE_NE = 27;
	public const int TERRAIN_RUBBLE_SW = 28;
	public const int TERRAIN_RUBBLE_SE = 29;
	public const int TERRAIN_RADIOACTIVE_NW = 30;
	public const int TERRAIN_RADIOACTIVE_NE = 31;
	public const int TERRAIN_RADIOACTIVE_SW = 32;
	public const int TERRAIN_RADIOACTIVE_SE = 33;
	public const int TERRAIN_ACID_NW = 34;
	public const int TERRAIN_ACID_NE = 35;
	public const int TERRAIN_ACID_SW = 36;
	public const int TERRAIN_ACID_SE = 37;
	public const int TERRAIN_HELLMOUTH_NW = 38;
	public const int TERRAIN_HELLMOUTH_NE = 39;
	public const int TERRAIN_HELLMOUTH_SW = 40;
	public const int TERRAIN_HELLMOUTH_SE = 41;
	public const int TERRAIN_FIRE_NW = 42;
	public const int TERRAIN_FIRE_NE = 43;
	public const int TERRAIN_FIRE_SW = 44;
	public const int TERRAIN_FIRE_SE = 45;
	public int[,] terrainMap = new int[MAP_CELLS, MAP_CELLS];
	public int[,] buildingMap = new int[MAP_CELLS, MAP_CELLS];
	public int[,] unitMap = new int[MAP_CELLS, MAP_CELLS];
	public int[,] monsterMap = new int[MAP_CELLS, MAP_CELLS];

	// ATTACK TYPES
	public const int ATTACK_TYPE_BRAWL = 0;
	public const int ATTACK_TYPE_BLAST = 1;
	public const int ATTACK_TYPE_POWER = 2;

	// Powers
	// # of powers
	public const int POWER_COUNT = 1;
	// Actual powers
	public const int POWER_FLYING = 0;


	public GameObject[] blueUnitSpawnZones = new GameObject[5];
	public GameObject[] redUnitSpawnZones = new GameObject[5];
	public GameObject[] blueMonsterSpawnZones = new GameObject[2];
	public GameObject[] redMonsterSpawnZones = new GameObject[2];
	public GameObject[] greenBuildingSpawnZones = new GameObject[TOTAL_GREEN_ZONES];
	public GameObject[] yellowBuildingSpawnZones
		= new GameObject[TOTAL_YELLOW_ZONES];

	private GameObject[] monsters = new GameObject[2];
	//private List<GameObject> units = new List<GameObject>();

	private bool[] monsterSpawned = { false, false };

	public enum AttackStatus { ChooseLeader, ChooseTarget, ChooseAssistants };
	public AttackStatus currentAttack;

	public GameObject okPopup;
	public Text okPopupText;

	public GameObject blueUnitBankPanel;
	public GameObject redUnitBankPanel;

	public GameObject chooseTurnTypeCanvas;
	public GameObject monsterTurnButton;
	public GameObject unitTurnButton;
	public Text chooseTurnText;

	public GameObject instructionPanel;
	public Text instructionText;

	public GameObject chooseAttackTypeCanvas;
	public GameObject chooseAttackDiceCanvas;
	public GameObject chooseAttackDicePanelText;

	public GameObject chooseAttackActionDicePanel;
	public GameObject chooseAttackBoostDicePanel;
	public GameObject chooseAttackPowerDicePanel;

	private DiceHandler dHandler;
	private UnitHandler uHandler;
	private BuildingHandler bHandler;

	public GameObject blueDoneButton;
	public GameObject redDoneButton;
	public GameObject blueClearButton;
	public GameObject redClearButton;
	public GameObject blueConfirmAttackButton;
	public GameObject redConfirmAttackButton;
	public GameObject brawlAttackButton;
	public GameObject blastAttackButton;
	public GameObject powerAttackButton;
	public GameObject attackDiceOkButton;
	public GameObject attackDiceActionPlusButton;
	public GameObject attackDiceActionMinusButton;
	public GameObject attackDicePowerPlusButton;
	public GameObject attackDicePowerMinusButton;

	private int minActionDiceForAttack = 0;
	private int maxActionDiceForAttack = 0;
	private int minPowerDiceForAttack = 0;
	private int maxPowerDiceForAttack = 0;


	void Awake()
	{
		//Application.targetFrameRate = 30;

		remainingGreenZones = TOTAL_GREEN_ZONES;
		remainingYellowZones = TOTAL_YELLOW_ZONES;

		currentPlayer = BLUE_PLAYER;
		firstPlayer = BLUE_PLAYER;
		prototypes[APARTMENT_BUILDING] = apartment_building;
		prototypes[TERRA_KHAN_ALPHA] = terra_khan_alpha;
		dHandler = GameObject.Find("ToolboxObj").GetComponent<DiceHandler>();
		uHandler = GameObject.Find("ToolboxObj").GetComponent<UnitHandler>();
		bHandler = GameObject.Find("ToolboxObj").GetComponent<BuildingHandler>();

		// Turn off all currently unneeded components.
		ActivateUnitSpawnZones(BLUE_PLAYER, false);
		ActivateUnitSpawnZones(RED_PLAYER, false);
		ActivateMonsterSpawnZones(BLUE_PLAYER, false);
		ActivateMonsterSpawnZones(RED_PLAYER, false);
		ActivateBuildingSpawnZones(GREEN_BUILDING, false);
		ActivateBuildingSpawnZones(YELLOW_BUILDING, false);

		// Set up maps.
		SetUpMaps();

		// PHASE STUFF //
		currentPhase = PHASE_CHOOSE_BUILDING;
		ActivateBuildingSpawnZones(GREEN_BUILDING, true);
		// TEMPORARY
		/*currentPhase = PHASE_UNIT_SPAWN;
		ActivateUnitSpawnZones(BLUE_PLAYER, true);
		unitsSpawned = false;
		AddUnitToMap(new Vector2Int(10, 10), RED_PLAYER);*/

		UpdatePhase(false);
	}

	public void SetUpMaps()
	{
		// Generic setup
		for (int i = 0; i < MAP_CELLS; i++)
		{
			for (int j = 0; j < MAP_CELLS; j++)
			{
				terrainMap[i, j] = TERRAIN_EMPTY;
				unitMap[i, j] = TERRAIN_EMPTY;
				buildingMap[i, j] = TERRAIN_EMPTY;
				monsterMap[i, j] = TERRAIN_EMPTY;

			}
		}
		// Map specific
		terrainMap[2, 4] = TERRAIN_ROUGHCOVER;
		terrainMap[2, 9] = TERRAIN_WATER;
		terrainMap[2, 10] = TERRAIN_WATER;
		terrainMap[3, 4] = TERRAIN_ROUGH;
		terrainMap[3, 9] = TERRAIN_ROUGHCOVER;
		terrainMap[3, 10] = TERRAIN_ROUGH;
		terrainMap[6, 2] = TERRAIN_ROUGHCOVER;
		terrainMap[6, 3] = TERRAIN_ROUGH;
		terrainMap[6, 8] = TERRAIN_ROUGHCOVER;
		terrainMap[7, 8] = TERRAIN_ROUGHCOVER;
		terrainMap[7, 13] = TERRAIN_WATER;
		terrainMap[7, 14] = TERRAIN_WATER;
		terrainMap[8, 6] = TERRAIN_ROUGHCOVER;
		terrainMap[8, 7] = TERRAIN_ROUGHCOVER;
		terrainMap[8, 9] = TERRAIN_ROUGHCOVER;
		terrainMap[8, 10] = TERRAIN_ROUGHCOVER;
		terrainMap[9, 2] = TERRAIN_WATER;
		terrainMap[9, 3] = TERRAIN_WATER;
		terrainMap[9, 8] = TERRAIN_ROUGHCOVER;
		terrainMap[10, 8] = TERRAIN_ROUGHCOVER;
		terrainMap[10, 13] = TERRAIN_ROUGH;
		terrainMap[10, 14] = TERRAIN_ROUGHCOVER;
		terrainMap[13, 6] = TERRAIN_ROUGH;
		terrainMap[13, 7] = TERRAIN_ROUGHCOVER;
		terrainMap[13, 12] = TERRAIN_ROUGH;
		terrainMap[14, 6] = TERRAIN_WATER;
		terrainMap[14, 7] = TERRAIN_WATER;
		terrainMap[14, 12] = TERRAIN_ROUGHCOVER;

	}

	public void AddUnitToMap(Vector2Int posn, int color)
	{
		if (posn.x < 0 || posn.x >= unitMap.GetLength(0)
			|| posn.y < 0 || posn.y >= unitMap.GetLength(1))
		{
			print("Error, added unit out of bounds - Toolbox/AddUnitToMap");
			return;
		}
		if (color == BLUE_PLAYER)
		{
			unitMap[posn.x, posn.y] = TERRAIN_BLUE_UNIT;
		}
		else
		{
			unitMap[posn.x, posn.y] = TERRAIN_RED_UNIT;
		}
	}

	public void AddBuildingToMap(Vector2Int nwCorner)
	{
		if (nwCorner.x < 0 || nwCorner.x >= unitMap.GetLength(0) - 1
			|| nwCorner.y <= 0 || nwCorner.y >= unitMap.GetLength(1))
		{
			print("Error, added building out of bounds - Toolbox/AddBuildingToMap");
			return;
		}
		buildingMap[nwCorner.x, nwCorner.y] = TERRAIN_BUILDING_NW;
		buildingMap[nwCorner.x + 1, nwCorner.y] = TERRAIN_BUILDING_NE;
		buildingMap[nwCorner.x, nwCorner.y - 1] = TERRAIN_BUILDING_SW;
		buildingMap[nwCorner.x + 1, nwCorner.y - 1] = TERRAIN_BUILDING_SE;
	}

	public void AddMonsterToMap(Vector2Int nwCorner, int color)
	{
		if (nwCorner.x < 0 || nwCorner.x >= unitMap.GetLength(0) - 1
			|| nwCorner.y <= 0 || nwCorner.y >= unitMap.GetLength(1))
		{
			print("Error, added building out of bounds - Toolbox/AddMonsterToMap");
			return;
		}
		if (color == BLUE_PLAYER)
		{
			monsterMap[nwCorner.x, nwCorner.y] = TERRAIN_BLUE_MONSTER_NW;
			monsterMap[nwCorner.x + 1, nwCorner.y] = TERRAIN_BLUE_MONSTER_NE;
			monsterMap[nwCorner.x, nwCorner.y - 1] = TERRAIN_BLUE_MONSTER_SW;
			monsterMap[nwCorner.x + 1, nwCorner.y - 1] = TERRAIN_BLUE_MONSTER_SE;
		}
		else
		{
			monsterMap[nwCorner.x, nwCorner.y] = TERRAIN_RED_MONSTER_NW;
			monsterMap[nwCorner.x + 1, nwCorner.y] = TERRAIN_RED_MONSTER_NE;
			monsterMap[nwCorner.x, nwCorner.y - 1] = TERRAIN_RED_MONSTER_SW;
			monsterMap[nwCorner.x + 1, nwCorner.y - 1] = TERRAIN_RED_MONSTER_SE;
		}


	}

	public void RemoveUnitFromMap(Vector2Int posn)
	{
		if (posn.x < 0 || posn.x >= unitMap.GetLength(0)
			|| posn.y < 0 || posn.y >= unitMap.GetLength(1))
		{
			print("Error, added unit out of bounds - Toolbox/RemoveUnitFromMap");
			return;
		}
		unitMap[posn.x, posn.y] = TERRAIN_EMPTY;
	}

	public void Spawn(int name, Vector3 posn)
	{
		if (name >= INDEX_BUILDING_MIN && name <= INDEX_BUILDING_MAX)
		{
			GameObject building = Instantiate(prototypes[name], posn, Quaternion.identity);
			building.GetComponent<Figure>().color = currentPlayer;
			GameObject border = Instantiate(largeBorders[currentPlayer], posn, Quaternion.identity);
			border.transform.parent = building.transform;
			bHandler.buildings.Add(building);
			AddBuildingToMap(GetNWCorner(building));
		}
		else if (name >= INDEX_MONSTER_MIN && name <= INDEX_MONSTER_MAX)
		{
			GameObject monster = Instantiate(prototypes[name], posn, Quaternion.identity);
			monster.GetComponent<Figure>().color = currentPlayer;
			GameObject border = Instantiate(largeBorders[currentPlayer], posn, Quaternion.identity);
			border.transform.parent = monster.transform;
			monsters[currentPlayer] = monster;
			AddMonsterToMap(GetNWCorner(monster), currentPlayer);
		}

	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	public void PromptPlayerForMoveType()
	{
		chooseTurnText.text = playerNames[currentPlayer] +
			", choose your turn type.";
		chooseTurnTypeCanvas.SetActive(true);
		if (currentPlayer == BLUE_PLAYER)
		{
			if (dHandler.blueActionDiceMonster.Count > 0)
			{
				monsterTurnButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				monsterTurnButton.GetComponent<Button>().interactable = false;
			}

			if (dHandler.blueActionDiceUnits.Count > 0)
			{
				unitTurnButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				unitTurnButton.GetComponent<Button>().interactable = false;
			}
		}
		else
		{
			if (dHandler.redActionDiceMonster.Count > 0)
			{
				monsterTurnButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				monsterTurnButton.GetComponent<Button>().interactable = false;
			}

			if (dHandler.redActionDiceUnits.Count > 0)
			{
				unitTurnButton.GetComponent<Button>().interactable = true;
			}
			else
			{
				unitTurnButton.GetComponent<Button>().interactable = false;
			}
		}
	}

	public void PromptPlayerForAttackType(GameObject attacker)
	{
		chooseAttackTypeCanvas.SetActive(true);
		// If it's a unit
		Unit unit = attacker.GetComponent<Unit>();
		if (unit)
		{
			if (unit.CanBrawl())
			{
				brawlAttackButton.SetActive(true);
			}
			else
			{
				brawlAttackButton.SetActive(false);
			}
			if (unit.CanBlast())
			{
				blastAttackButton.SetActive(true);
			}
			else
			{
				blastAttackButton.SetActive(false);
			}
			powerAttackButton.SetActive(false);
		}
		// Else if it's a monster.
		Monster monster = attacker.GetComponent<Monster>();
		if (monster)
		{
			if (monster.CanBrawl())
			{
				brawlAttackButton.SetActive(true);
			}
			else
			{
				brawlAttackButton.SetActive(false);
			}
			if (monster.CanBlast())
			{
				blastAttackButton.SetActive(true);
			}
			else
			{
				blastAttackButton.SetActive(false);
			}
			powerAttackButton.SetActive(false);
		}
	}

	private void DoPhaseChooseBuilding()
	{
		if (remainingGreenZones > 0)
		{
			instructionText.text = playerNames[currentPlayer] +
				", place a building in a green zone.";
		}
		else if (remainingYellowZones > 0)
		{
			// First time.
			if (remainingYellowZones == TOTAL_YELLOW_ZONES)
			{
				ActivateBuildingSpawnZones(YELLOW_BUILDING, true);
			}
			instructionText.text = playerNames[currentPlayer] +
				", place a building in a yellow zone.";
		}
		else
		{
			ActivateBuildingSpawnZones(YELLOW_BUILDING, false);
			ActivateMonsterSpawnZones(BLUE_PLAYER, true);
			ActivateMonsterSpawnZones(RED_PLAYER, true);
			// Prompt first player for their move
			currentPhase = PHASE_CHOOSE_MONSTER_SPAWN;
			currentPlayer = firstPlayer;
			UpdatePhase(false);
			return;
		}
	}

	private void DoPhaseChooseMonsterSpawn()
	{
		if (monsterSpawned[BLUE_PLAYER] && monsterSpawned[RED_PLAYER])
		{
			ActivateMonsterSpawnZones(BLUE_PLAYER, false);
			ActivateMonsterSpawnZones(RED_PLAYER, false);
			currentPlayer = firstPlayer;
			PromptPlayerForMoveType();
			return;
		}
		instructionText.text = playerNames[currentPlayer] + ", place your monster.";
		monsterSpawned[currentPlayer] = true;
	}

	private void DoPhaseUnitSpawn()
	{
		instructionText.text = playerNames[currentPlayer]
			+ ", choose a unit from your bank.";
		if (currentPlayer == BLUE_PLAYER)
		{

			blueDoneButton.SetActive(true);
			blueClearButton.SetActive(true);
		}
		else
		{
			redDoneButton.SetActive(true);
			redClearButton.SetActive(true);
		}
	}

	private void DoPhaseUnitMove()
	{
		instructionText.text = playerNames[currentPlayer]
			+ ", choose a unit to move.";
		if (currentPlayer == BLUE_PLAYER)
		{
			blueDoneButton.SetActive(true);
			//blueClearButton.SetActive(true);
		}
		else
		{
			redDoneButton.SetActive(true);
			//redClearButton.SetActive(true);
		}
	}

	public void DoPhaseUnitAttack()
	{
		uHandler.ClearTargetingCells();
		uHandler.ClearPossibleAssistants();
		uHandler.attackLeader = null;
		uHandler.attackTarget = null;
		uHandler.attackAssistants.Clear();
		currentAttack = AttackStatus.ChooseLeader;
		instructionText.text = playerNames[currentPlayer]
			+ ", choose a leader for the attack.";
		if (currentPlayer == BLUE_PLAYER)
		{
			blueDoneButton.SetActive(true);
			blueClearButton.SetActive(true);
			blueConfirmAttackButton.SetActive(false);
		}
		else
		{
			redDoneButton.SetActive(true);
			redClearButton.SetActive(true);
			redConfirmAttackButton.SetActive(false);
		}
	}

	public void SwitchPlayer()
	{
		if (currentPlayer == BLUE_PLAYER)
		{
			currentPlayer = RED_PLAYER;
		}
		else
		{
			currentPlayer = BLUE_PLAYER;
		}
	}

	public void UpdatePhase(bool switchPlayer)
	{
		// Switch player
		if (switchPlayer)
		{
			SwitchPlayer();
		}
		// Act based on current phase
		switch (currentPhase)
		{
			case PHASE_CHOOSE_BUILDING:
				DoPhaseChooseBuilding();
				break;
			case PHASE_CHOOSE_MONSTER_SPAWN:
				DoPhaseChooseMonsterSpawn();
				break;
			case PHASE_UNIT_SPAWN:
				DoPhaseUnitSpawn();
				break;
			case PHASE_UNIT_MOVE:
				DoPhaseUnitMove();
				break;
			case PHASE_UNIT_ATTACK:
				DoPhaseUnitAttack();
				break;
			case PHASE_POWER_UP:
				print("POWER UP");
				break;
			case PHASE_HYPER_UP:
				print("HYPER UP");
				break;
			case PHASE_MONSTER_MOVE:
				print("MONSTER MOVE");
				break;
			case PHASE_MONSTER_ATTACK:
				print("MONSTER ATTACK");
				break;
			default:
				print("Error in Toolbox.UpdatePhase() - default case");
				break;
		}
	}

	public void DoDoneButtonStuff(int color)
	{
		if (currentPhase == PHASE_UNIT_SPAWN)
		{
			uHandler.CommitSpawns(color);
			okPopupText.text = playerNames[currentPlayer] + "\n UNIT MOVE PHASE";
			okPopup.SetActive(true);

			currentPhase = PHASE_UNIT_MOVE;
			uHandler.SetAllUnitsCanMove();
			ActivateUnitSpawnZones(currentPlayer, false);

			UpdatePhase(false);
		}
		else if (currentPhase == PHASE_UNIT_MOVE)
		{
			uHandler.CommitMoves(color);
			currentPhase = PHASE_UNIT_ATTACK;
			uHandler.SetAllUnitsCanAttack();
			okPopupText.text = playerNames[currentPlayer]
				+ "\n UNIT ATTACK PHASE";
			okPopup.SetActive(true);
			UpdatePhase(false);
		}
		else if (currentPhase == PHASE_UNIT_ATTACK)
		{
			currentPhase = PHASE_POWER_UP;
			SwitchPlayer();
			PromptPlayerForMoveType();
			return;
		}
	}

	public void OnClickButton(string buttonName)
	{
		// Regardless, lose the selected unit.
		uHandler.SelectUnit(null);

		if (buttonName == "BlueDoneButton")
		{
			blueDoneButton.SetActive(false);
			blueClearButton.SetActive(false);
			DoDoneButtonStuff(BLUE_PLAYER);
		}
		else if (buttonName == "BlueClearButton")
		{
			if (currentPhase == PHASE_UNIT_SPAWN)
			{
				uHandler.ClearSpawns(BLUE_PLAYER);
			}
			else if (currentPhase == PHASE_UNIT_ATTACK)
			{
				uHandler.ClearAttack();
			}
			UpdatePhase(false);
		}
		else if (buttonName == "RedDoneButton")
		{
			redDoneButton.SetActive(false);
			redClearButton.SetActive(false);
			DoDoneButtonStuff(RED_PLAYER);
		}
		else if (buttonName == "RedClearButton")
		{
			if (currentPhase == PHASE_UNIT_SPAWN)
			{
				uHandler.ClearSpawns(RED_PLAYER);
			}
			else if (currentPhase == PHASE_UNIT_ATTACK)
			{
				uHandler.ClearAttack();
			}
			UpdatePhase(false);
		}
		else if (buttonName == "MonsterTurnButton")
		{
			currentPhase = PHASE_POWER_UP;
			chooseTurnTypeCanvas.SetActive(false);
			UpdatePhase(false);

		}
		else if (buttonName == "UnitTurnButton")
		{
			currentPhase = PHASE_UNIT_SPAWN;
			chooseTurnTypeCanvas.SetActive(false);
			ActivateUnitSpawnZones(currentPlayer, true);
			okPopupText.text = playerNames[currentPlayer] + "\n UNIT SPAWN PHASE";
			okPopup.SetActive(true);
			UpdatePhase(false);
		}
		else if (buttonName == "BlueConfirmAttackButton")
		{
			// Expand later.
			blueConfirmAttackButton.SetActive(false);
			uHandler.CommitAttack();
			//UpdatePhase(false);
		} 
		else if (buttonName == "RedConfirmAttackButton")
		{
			redConfirmAttackButton.SetActive(false);
			uHandler.CommitAttack();
			//UpdatePhase(false);
		}
		else if (buttonName == "BrawlAttackButton")
		{
			if (currentPhase == PHASE_UNIT_ATTACK)
			{
				uHandler.HandleAttackType(ATTACK_TYPE_BRAWL);
			}
			chooseAttackTypeCanvas.SetActive(false);

		}
		else if (buttonName == "BlastAttackButton")
		{
			if (currentPhase == PHASE_UNIT_ATTACK)
			{
				uHandler.HandleAttackType(ATTACK_TYPE_BLAST);
			}
			chooseAttackTypeCanvas.SetActive(false);
		}
		else if (buttonName == "PowerAttackButton")
		{
			chooseAttackTypeCanvas.SetActive(false);
			// IMPLEMENT
		}
		else if (buttonName == "AttackDiceOkButton")
		{
			if (attackDiceOkButton.GetComponentInChildren<Text>().text.Equals("Roll"))
			{
				HandleAttackRoll();
			}
			else
			{
				HandleAttackFinished();
				UpdatePhase(false);
			}

		}
		else if (buttonName == "AttackDiceActionMinusButton")
		{
			if (currentPhase == PHASE_UNIT_ATTACK)
			{
				dHandler.TransferActionDiceToAttack(currentPlayer, UNIT, -1);
			}
			else
			{
				dHandler.TransferActionDiceToAttack(currentPlayer, MONSTER, -1);
			}
			// Turn off the minus sign if we're at our minimum dice.
			if ((currentPlayer == BLUE_PLAYER 
				&& dHandler.blueActionDiceAttack.Count == minActionDiceForAttack)
				|| (currentPlayer == RED_PLAYER
				&& dHandler.redActionDiceAttack.Count == minActionDiceForAttack))
			{
				attackDiceActionMinusButton.SetActive(false);
			}
			attackDiceActionPlusButton.SetActive(true);

		}
		else if (buttonName == "AttackDiceActionPlusButton")
		{
			if (currentPhase == PHASE_UNIT_ATTACK)
			{
				dHandler.TransferActionDiceToAttack(currentPlayer, UNIT, 1);
			}
			else
			{
				dHandler.TransferActionDiceToAttack(currentPlayer, MONSTER, 1);
			}
			// Turn off the plus sign if we're at our maximum dice.
			if ((currentPlayer == BLUE_PLAYER
				&& dHandler.blueActionDiceAttack.Count == maxActionDiceForAttack)
				|| (currentPlayer == RED_PLAYER
				&& dHandler.redActionDiceAttack.Count == maxActionDiceForAttack))
			{
				attackDiceActionPlusButton.SetActive(false);
			}
			attackDiceActionMinusButton.SetActive(true);


		}
		else if (buttonName == "AttackDicePowerMinusButton")
		{

		}
		else if (buttonName == "AttackDicePowerPlusButton")
		{

		}
		else
		{
			print("Typo somewhere in Toolbox/OnClickButton");
		}
	}

	public void ActivateUnitSpawnZones(int color, bool turnOn)
	{
		if (color == BLUE_PLAYER)
		{
			foreach (GameObject zone in blueUnitSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
		else
		{
			foreach (GameObject zone in redUnitSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
	}

	public void ActivateMonsterSpawnZones(int color, bool turnOn)
	{
		if (color == BLUE_PLAYER)
		{
			foreach (GameObject zone in blueMonsterSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
		else
		{
			foreach (GameObject zone in redMonsterSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
	}

	public void ActivateBuildingSpawnZones(int color, bool turnOn)
	{
		if (color == GREEN_BUILDING)
		{
			foreach (GameObject zone in greenBuildingSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
		else
		{
			foreach (GameObject zone in yellowBuildingSpawnZones)
			{
				zone.GetComponent<SpawnZone>().SetActiveZone(turnOn);
			}
		}
	}

	// Translate unit map coordinates to cell, w/ (0,0) in lower left.
	public Vector2Int GetCell(GameObject obj)
	{
		GameObject map = GameObject.Find("Map");
		SpriteRenderer render = map.GetComponent<SpriteRenderer>();
		Vector3 scale = map.transform.localScale;
		float mapHeight = render.bounds.max.y - render.bounds.min.y;
		float mapWidth = render.bounds.max.x - render.bounds.min.x;
		Vector2 cellSize = new Vector2(mapWidth / MAP_CELLS, mapHeight / MAP_CELLS);

		Vector2 posn = new Vector2(obj.transform.position.x, obj.transform.position.y);
		int xCell = (int)((posn.x - render.bounds.min.x) / cellSize.x);
		int yCell = (int)((posn.y - render.bounds.min.y) / cellSize.y);
		Vector2Int cellPosition = new Vector2Int(xCell, yCell);
		return cellPosition;
	}

	// Translate a 4-square obj nw corner cell, w/ (0,0) in lower left of map.
	public Vector2Int GetNWCorner(GameObject obj)
	{
		GameObject map = GameObject.Find("Map");
		SpriteRenderer render = map.GetComponent<SpriteRenderer>();
		Vector3 scale = map.transform.localScale;
		float mapHeight = render.bounds.max.y - render.bounds.min.y;
		float mapWidth = render.bounds.max.x - render.bounds.min.x;
		Vector2 cellSize = new Vector2(mapWidth / MAP_CELLS, mapHeight / MAP_CELLS);

		Vector2 posn = new Vector2(obj.transform.position.x,
			obj.transform.position.y);
		// Shift from the center of the building to the NW corner.
		int xCell = (int)((posn.x - render.bounds.min.x - (cellSize.x / 2))
			/ cellSize.x);
		int yCell = (int)((posn.y - render.bounds.min.y + (cellSize.y / 2))
			/ cellSize.y);
		Vector2Int cellPosition = new Vector2Int(xCell, yCell);
		return cellPosition;
	}

	// Given any cell in a building, get the NW corner of it.
	public Vector2Int GetNWCorner(Vector2Int cell)
	{
		if (buildingMap[cell.x, cell.y] == TERRAIN_BUILDING_NE)
		{
			return new Vector2Int(cell.x - 1, cell.y);

		}
		else if (buildingMap[cell.x, cell.y] == TERRAIN_BUILDING_NW)
		{
			return cell;
		}
		else if (buildingMap[cell.x, cell.y] == TERRAIN_BUILDING_SE)
		{
			return new Vector2Int(cell.x - 1, cell.y + 1);
		}
		else if (buildingMap[cell.x, cell.y] == TERRAIN_BUILDING_SW)
		{
			return new Vector2Int(cell.x, cell.y + 1);
		}
		return new Vector2Int(-1, -1);
	}

	// Return true if the given cell is part of a building.
	public bool CellIsBuilding(Vector2Int cell)
	{
		int mapEntry = buildingMap[cell.x, cell.y];
		return (mapEntry == TERRAIN_BUILDING_NE || mapEntry == TERRAIN_BUILDING_NW
			|| mapEntry == TERRAIN_BUILDING_SE || mapEntry == TERRAIN_BUILDING_SW);
	}

	// Return true if the given cell is a unit.
	public bool CellIsUnit(int color, Vector2Int cell)
	{
		if (color == Toolbox.BLUE_PLAYER)
		{
			return (unitMap[cell.x, cell.y] == Toolbox.TERRAIN_BLUE_UNIT);

		}
		else
		{
			return (unitMap[cell.x, cell.y] == Toolbox.TERRAIN_RED_UNIT);

		}
	}
	
	// Return true if the given cell is part of a monster of the given color.
	public bool CellIsMonster(int color, Vector2Int cell)
	{
		int mapEntry = monsterMap[cell.x, cell.y];
		if (color == BLUE_PLAYER)
		{
			return (mapEntry == TERRAIN_BLUE_MONSTER_NE 
				|| mapEntry == TERRAIN_BLUE_MONSTER_NW
				|| mapEntry == TERRAIN_BLUE_MONSTER_SE 
				|| mapEntry == TERRAIN_BLUE_MONSTER_SW);
		}
		else
		{
			return (mapEntry == TERRAIN_RED_MONSTER_NE
				|| mapEntry == TERRAIN_RED_MONSTER_NW
				|| mapEntry == TERRAIN_RED_MONSTER_SE
				|| mapEntry == TERRAIN_RED_MONSTER_SW);

		}
	}

	// Translate (x,y) cell pair to center of a map cell, w/ (0,0) in lower left.
	public Vector3 TranslateCellToMap(Vector2Int cell)
	{
		GameObject map = GameObject.Find("Map");
		SpriteRenderer render = map.GetComponent<SpriteRenderer>();
		float mapHeight = render.bounds.max.y - render.bounds.min.y;
		float mapWidth = render.bounds.max.x - render.bounds.min.x;
		Vector2 cellSize = new Vector2(mapWidth / MAP_CELLS, mapHeight / MAP_CELLS);
		// The .5 should center the result.
		return new Vector3(render.bounds.min.x + ((cell.x + 0.5f) * cellSize.x),
			render.bounds.min.y + ((cell.y + 0.5f) * cellSize.y), 0);
	}

	// Given the NW corner of a building or monster, return its centerpoint.
	public Vector3 TranslateNWCornerToMap(Vector2Int cell)
	{
		GameObject map = GameObject.Find("Map");
		SpriteRenderer render = map.GetComponent<SpriteRenderer>();
		float mapHeight = render.bounds.max.y - render.bounds.min.y;
		float mapWidth = render.bounds.max.x - render.bounds.min.x;
		Vector2 cellSize = new Vector2(mapWidth / MAP_CELLS, mapHeight / MAP_CELLS);
		// The +1 should center the x result for a 2x2 object
		return new Vector3(render.bounds.min.x + ((cell.x + 1) * cellSize.x),
			render.bounds.min.y + ((cell.y) * cellSize.y), 0);
	}

	// Get the shortest distance between the two objects.
	public int GetRange(GameObject source, GameObject target)
	{
		Vector2Int sourceCell = GetClosestCell(source, target);
		Vector2Int targetCell = GetClosestCell(target, source);
		if (sourceCell.x == targetCell.x || sourceCell.y == targetCell.y)
		{
			return (Mathf.Abs(sourceCell.x - targetCell.x)
				+ Mathf.Abs(sourceCell.y - targetCell.y));
		}
		// 1 free diagonal
		return (Mathf.Abs(sourceCell.x - targetCell.x)
			+ Mathf.Abs(sourceCell.y - targetCell.y) - 1);
	}

	// Determine whether the objects are within the min and max range bands.
	public bool InRange(GameObject source, GameObject target, int minRange,
		int maxRange)
	{
		int range = GetRange(source, target);
		return (minRange <= range && range <= maxRange);
	}

	public Vector2Int GetClosestCell(GameObject from, GameObject to)
	{
		// If from is a unit, he only has one cell; report that.
		if (from.GetComponent<Unit>())
		{
			return GetCell(from);
		}
		// Otherwise, it's a big object. Get its NW corner.
		Vector2Int corner = GetNWCorner(from);
		// If from is west of to, we need the east side of from
		if (from.transform.position.x < to.transform.position.x)
		{
			corner.x += 1;
		}
		// If from is north of to, we need the south side of from.
		if (from.transform.position.y > to.transform.position.y)
		{
			corner.y -= 1;
		}
		return corner;
	}

	public GameObject GetMonster(int color)
	{
		return monsters[color];
	}

	public int GetEnemyColor(int color)
	{
		return (color == BLUE_PLAYER) ? RED_PLAYER : BLUE_PLAYER;
	}

	public void PresentAttackResolutionWindow(int unitOrMonster, int attackType, 
		int minActionDice, int maxActionDice, int boostDice, int minPowerDice, int maxPowerDice)
	{
		chooseAttackDicePanelText.GetComponent<Text>().text = "How many dice will you spend?";
		attackDiceOkButton.GetComponentInChildren<Text>().text = "Roll";
		// Turn on and off relevant buttons
		chooseAttackDiceCanvas.SetActive(true);
		attackDiceActionMinusButton.SetActive(false);
		// Save values.
		minActionDiceForAttack = minActionDice;
		maxActionDiceForAttack = maxActionDice;
		minPowerDiceForAttack = minPowerDice;
		maxPowerDiceForAttack = maxPowerDice;

		// Adjust buttons.
		if (minActionDice < maxActionDice)
		{
			attackDiceActionPlusButton.SetActive(true);
		}
		else
		{
			attackDiceActionPlusButton.SetActive(false);
		}
		if (unitOrMonster == UNIT)
		{
			chooseAttackPowerDicePanel.SetActive(false);
		}
		else
		{
			chooseAttackPowerDicePanel.SetActive(true);
		}
		attackDicePowerMinusButton.SetActive(false);
		if (minPowerDice < maxPowerDice)
		{
			attackDicePowerPlusButton.SetActive(true);
		}
		else
		{
			attackDicePowerPlusButton.SetActive(false);
		}
		// Move and spawn relevant dice.
		dHandler.SetDiceForAttack(currentPlayer, unitOrMonster, minActionDice, boostDice, 0);

	}

	public void HandleAttackRoll()
	{
		dHandler.RollAttackDice();
		int strikes = dHandler.GetAttackStrikes();
		chooseAttackDicePanelText.GetComponent<Text>().text = "You rolled " + strikes
			+ " strikes.";
		attackDiceOkButton.GetComponentInChildren<Text>().text = "OK";
		attackDiceActionMinusButton.SetActive(false);
		attackDiceActionPlusButton.SetActive(false);
		attackDicePowerMinusButton.SetActive(false);
		attackDicePowerPlusButton.SetActive(false);
	}

	public void HandleAttackFinished()
	{
		Figure target = uHandler.attackTarget.GetComponent<Figure>();
		int strikes = dHandler.GetAttackStrikes();
		chooseAttackDiceCanvas.SetActive(false);
		// Final clean up for dice
		dHandler.ClearDiceForAttack();
		// Final clean up for units.
		uHandler.FinalCleanupForAttack();
		// Resolve damage
		if (strikes >= target.defense)
		{
			target.health--;
			if (target.health == 0)
			{
				if (target.GetComponent<Unit>())
				{
					uHandler.ReturnToBank(target.gameObject);
					// Pay a power die to the attacker.
					dHandler.AddPowerDice(1, currentPlayer);
				}
				else if (target.GetComponent<Building>())
				{
					bHandler.DestroyBuilding(target.gameObject);
					// Pay a power die to the attacker.
					dHandler.AddPowerDice(1, currentPlayer);
				}
				else if (target.GetComponent<Monster>())
				{
					// IMPLEMENT
					// mHandler.KillMonster(target.gameObject);
				}
			}
		}
		
	}
}