﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Unit : Figure, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

	public const int GTANK_GRUNT = 0;

	public bool[] powers = new bool[Toolbox.POWER_COUNT];
	
	public int unitType;
	public int cost;
	public int speed;
	public int range;
	public int maxBlast;
	public int blastBoost;
	public int maxBrawl;
	public int brawlBoost;
	public bool hasMoved = false;
	public bool hasAttacked = false;


	public SpriteRenderer render;
	public Vector3 originalScale;
	//protected Toolbox tbox;
	public bool selected = false;
	protected UnitHandler uHandler;
	public bool gray = false;

	public void OnPointerEnter(PointerEventData eventData)
	{
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}

	// Use this for initialization
	void Start()
	{
		DoInitialization();
	}

	protected void DoInitialization()
	{
		render = GetComponent<SpriteRenderer>();
		//tbox = GameObject.Find("ToolboxObj").GetComponent<Toolbox>();
		uHandler = GameObject.Find("ToolboxObj").GetComponent<UnitHandler>();
		
		for (int i = 0; i < powers.Length; i++)
		{
			powers[i] = false;
		}
		health = 1;

	}

	public void OnPointerClick(PointerEventData eventData)
	{
		// Send message to UnitHandler
		uHandler.SelectUnit(this);
	}

	public void Select()
	{
		GameObject highlight = Instantiate(uHandler.unitHighlight, 
			transform.position, Quaternion.identity);
		highlight.GetComponent<SpriteRenderer>().sortingLayerName
			= render.sortingLayerName;
		highlight.transform.parent = transform;
		highlight.GetComponent<SpriteRenderer>().transform.localScale 
			= new Vector3(1,1,0);
		selected = true;
	}

	public void Deselect()
	{
		// Ignore if not selected
		if (!selected)
		{
			return;
		}
		Transform t = transform.Find("unit_highlight(Clone)");
		if (t)
		{
			Destroy(t.gameObject);
		}
		selected = false;
	}

	public void GrayOut()
	{
		render.color = new Color(1f, 1f, 1f, 0.8f);
		gray = true;
	}

	public void UnGray()
	{
		render.color = new Color(1f, 1f, 1f, 1f);
		gray = false;
	}

	public bool CanEnterWater()
	{
		return powers[Toolbox.POWER_FLYING];
	}

	public bool CanEnterEnemyUnit()
	{
		return powers[Toolbox.POWER_FLYING];
	}

	public bool CanEnterBuilding()
	{
		return powers[Toolbox.POWER_FLYING];
	}

	public bool CanBlast()
	{
		return maxBlast > 0;
	}

	public bool CanBrawl()
	{
		return maxBrawl > 0;
	}
}
