using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GtankGrunt : Unit {

    void Start()
    {
		base.DoInitialization();
        unitType = GTANK_GRUNT;
        cost = 1;
		speed = 4;
		defense = 4;
		range = 5;
		maxBlast = 2;
		blastBoost = 1;
		maxBrawl = 1;
		brawlBoost = 0;
    }
}

