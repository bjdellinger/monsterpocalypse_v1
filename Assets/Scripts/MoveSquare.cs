﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveSquare : MonoBehaviour, IPointerClickHandler {

	private UnitHandler uHandler;
	private Toolbox tbox;

	public void OnPointerClick(PointerEventData eventData)
	{
		Vector2Int cell = tbox.GetCell(this.gameObject);
		uHandler.MoveSelectedUnit(cell);
	}

	// Use this for initialization
	void Start () {
		uHandler = GameObject.Find("ToolboxObj").GetComponent<UnitHandler>();
		tbox = GameObject.Find("ToolboxObj").GetComponent<Toolbox>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
